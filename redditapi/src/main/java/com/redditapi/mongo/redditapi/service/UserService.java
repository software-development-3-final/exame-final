package com.redditapi.mongo.redditapi.service;

import com.redditapi.mongo.redditapi.model.DTO.UserDTO;
import com.redditapi.mongo.redditapi.repository.UserRepository;
import com.redditapi.mongo.redditapi.utils.ApiResponse;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public ApiResponse<UserDTO> createUser(@Valid UserDTO user) {
        if (StringUtils.isEmpty(user.getName()) || StringUtils.isEmpty(user.getLogin()) || StringUtils.isEmpty(user.getPassword())) {
            return new ApiResponse<>(400, "Name, login, and password cannot be empty", null);
        }

        if (userRepository.findByLogin(user.getLogin()) != null) {
            return new ApiResponse<>(400, "Login already exists", null);
        }

        user.setCreatedAt(new Date());
        return new ApiResponse<>(201, "User created successfully", userRepository.save(user));
    }

    public ApiResponse<List<UserDTO>> getAllUsers() {
        return new ApiResponse<>(200, "Success", userRepository.findAll());
    }

    public ApiResponse<UserDTO> getUserById(String id) {
        Optional<UserDTO> optionalUser = userRepository.findById(id);
        return optionalUser.map(user -> new ApiResponse<>(200, "Success", user))
                .orElseGet(() -> new ApiResponse<>(404, "User not found", null));
    }

    public ApiResponse<UserDTO> updateUser(String id, @Valid UserDTO user) {
        UserDTO existingUser = getUserById(id).getData();
        if (existingUser == null) {
            return new ApiResponse<>(404, "User not found", null);
        }

        if (!existingUser.getLogin().equals(user.getLogin()) && userRepository.findByLogin(user.getLogin()) != null) {
            return new ApiResponse<>(400, "Login already exists", null);
        }

        existingUser.setName(user.getName());
        existingUser.setLogin(user.getLogin());
        existingUser.setPassword(user.getPassword());
        return new ApiResponse<>(200, "User updated successfully", userRepository.save(existingUser));
    }

    public ApiResponse<Void> deleteUser(String id) {
        userRepository.deleteById(id);
        return new ApiResponse<>(204, "User deleted successfully", null);
    }
}