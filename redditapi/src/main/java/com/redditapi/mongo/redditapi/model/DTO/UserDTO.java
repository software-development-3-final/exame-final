package com.redditapi.mongo.redditapi.model.DTO;

import lombok.Data;
import org.mindrot.jbcrypt.BCrypt;

import java.util.Date;

@Data
public class UserDTO {
    private String id;

    private String name;

    private String login;

    private String password;

    private Date createdAt;

    public void setPassword(String password) {
        this.password = BCrypt.hashpw(password, BCrypt.gensalt());
    }
}
