package com.redditapi.mongo.redditapi.model.DTO;

import lombok.Data;

import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
public class IdeaDTO {
    @Id
    private String id;

    private String description;

    private String userId;

    private String topicId;

    private Date createdAt;

}
