package com.redditapi.mongo.redditapi.model.DTO;

import lombok.Data;
import org.springframework.data.annotation.Id;
import java.util.Date;


@Data
public class TopicDTO {
    @Id
    private String id;

    private String title;

    private String description;

    private Date createdAt;

}
