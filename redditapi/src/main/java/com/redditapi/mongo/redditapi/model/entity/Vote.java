package com.redditapi.mongo.redditapi.model.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Document(collection = "votes")
public class Vote {

    @Id
    private String id;
    @DBRef
    private Idea idea;
    @DBRef
    private User user;
    private int voteValue;
    private Date createdAt;
}
