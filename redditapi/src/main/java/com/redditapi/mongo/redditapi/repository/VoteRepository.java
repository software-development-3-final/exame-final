package com.redditapi.mongo.redditapi.repository;

import com.redditapi.mongo.redditapi.model.DTO.VoteDTO;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface VoteRepository extends MongoRepository<VoteDTO, String> {
}
