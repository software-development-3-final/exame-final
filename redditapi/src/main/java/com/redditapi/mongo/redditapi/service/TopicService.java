package com.redditapi.mongo.redditapi.service;

import com.redditapi.mongo.redditapi.model.DTO.TopicDTO;
import com.redditapi.mongo.redditapi.repository.TopicRepository;
import com.redditapi.mongo.redditapi.service.validation.ValidationService;
import com.redditapi.mongo.redditapi.utils.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TopicService {

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private ValidationService validationService;

    public ApiResponse<TopicDTO> createTopic(TopicDTO topic) {
        ApiResponse<Void> validationResponse = validationService.validateTopic(topic);
        if (validationResponse.getStatus() != 200) {
            return new ApiResponse<>(validationResponse.getStatus(), validationResponse.getMessage(), null);
        }

        topic.setCreatedAt(new Date());
        return new ApiResponse<>(201, "Topic created successfully", topicRepository.save(topic));
    }

    public ApiResponse<List<TopicDTO>> getAllTopics() {
        return new ApiResponse<>(200, "Success", topicRepository.findAll());
    }

    public ApiResponse<TopicDTO> getTopicById(String id) {
        Optional<TopicDTO> optionalTopic = topicRepository.findById(id);
        return optionalTopic.map(topic -> new ApiResponse<>(200, "Success", topic))
                .orElseGet(() -> new ApiResponse<>(404, "Topic not found", null));
    }

    public ApiResponse<TopicDTO> updateTopic(String id, TopicDTO topic) {
        TopicDTO existingTopic = getTopicById(id).getData();
        if (existingTopic == null) {
            return new ApiResponse<>(404, "Topic not found", null);
        }

        ApiResponse<Void> validationResponse = validationService.validateTopic(topic);
        if (validationResponse.getStatus() != 200) {
            return new ApiResponse<>(validationResponse.getStatus(), validationResponse.getMessage(), null);
        }

        existingTopic.setTitle(topic.getTitle());
        existingTopic.setDescription(topic.getDescription());
        return new ApiResponse<>(200, "Topic updated successfully", topicRepository.save(existingTopic));
    }

    public ApiResponse<Void> deleteTopic(String id) {
        topicRepository.deleteById(id);
        return new ApiResponse<>(204, "Topic deleted successfully", null);
    }
}
