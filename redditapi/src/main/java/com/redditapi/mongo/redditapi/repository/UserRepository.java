package com.redditapi.mongo.redditapi.repository;

import com.redditapi.mongo.redditapi.model.DTO.UserDTO;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<UserDTO, String> {
    UserDTO findByLogin(String login);
}
