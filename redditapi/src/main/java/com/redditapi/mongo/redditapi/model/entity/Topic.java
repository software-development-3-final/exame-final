package com.redditapi.mongo.redditapi.model.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Document(collection = "topics")
public class Topic {

    @Id
    private String id;
    private String title;
    private String description;
    @DBRef
    private User user;
    private Date createdAt;

}
