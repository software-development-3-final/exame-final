package com.redditapi.mongo.redditapi.controller;

import com.redditapi.mongo.redditapi.model.DTO.IdeaDTO;
import com.redditapi.mongo.redditapi.service.IdeaService;
import com.redditapi.mongo.redditapi.utils.ApiResponse;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ideas")
@Validated
public class IdeaController {

    @Autowired
    private IdeaService ideaService;

    @PostMapping
    public ApiResponse<IdeaDTO> createIdea(@Valid @RequestBody IdeaDTO idea) {
        return ideaService.createIdea(idea);
    }

    @GetMapping
    public ApiResponse<List<IdeaDTO>> getAllIdeas() {
        return ideaService.getAllIdeas();
    }

    @GetMapping("/{id}")
    public ApiResponse<IdeaDTO> getIdeaById(@PathVariable String id) {
        return ideaService.getIdeaById(id);
    }

    @PutMapping("/{id}")
    public ApiResponse<IdeaDTO> updateIdea(@PathVariable String id, @Valid @RequestBody IdeaDTO idea) {
        return ideaService.updateIdea(id, idea);
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> deleteIdea(@PathVariable String id) {
        return ideaService.deleteIdea(id);
    }
}
