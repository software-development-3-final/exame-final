package com.redditapi.mongo.redditapi.repository;

import com.redditapi.mongo.redditapi.model.DTO.IdeaDTO;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface IdeaRepository extends MongoRepository<IdeaDTO, String> {
}
