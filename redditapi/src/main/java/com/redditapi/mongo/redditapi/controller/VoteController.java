package com.redditapi.mongo.redditapi.controller;

import com.redditapi.mongo.redditapi.model.DTO.VoteDTO;
import com.redditapi.mongo.redditapi.service.VoteService;
import com.redditapi.mongo.redditapi.utils.ApiResponse;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;



import java.util.List;

@RestController
@RequestMapping("/votes")
@Validated
public class VoteController {

    @Autowired
    private VoteService voteService;

    @PostMapping
    public ApiResponse<VoteDTO> createVote(@Valid @RequestBody VoteDTO vote) {
        return voteService.createVote(vote);
    }

    @GetMapping
    public ApiResponse<List<VoteDTO>> getAllVotes() {
        return voteService.getAllVotes();
    }


    @GetMapping("/{id}")
    public ApiResponse<VoteDTO> getVoteById(@PathVariable String id) {
        return voteService.getVoteById(id);
    }


    @DeleteMapping("/{id}")
    public ApiResponse<Void> deleteVote(@PathVariable String id) {
        return voteService.deleteVote(id);
    }

}
