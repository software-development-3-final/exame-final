package com.redditapi.mongo.redditapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedditapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedditapiApplication.class, args);
	}

}
