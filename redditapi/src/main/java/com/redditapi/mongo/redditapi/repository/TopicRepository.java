package com.redditapi.mongo.redditapi.repository;

import com.redditapi.mongo.redditapi.model.DTO.TopicDTO;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TopicRepository extends MongoRepository<TopicDTO, String> {
}
