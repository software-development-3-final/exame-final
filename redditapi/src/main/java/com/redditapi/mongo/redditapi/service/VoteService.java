package com.redditapi.mongo.redditapi.service;

import com.redditapi.mongo.redditapi.model.DTO.VoteDTO;
import com.redditapi.mongo.redditapi.repository.VoteRepository;
import com.redditapi.mongo.redditapi.service.validation.ValidationService;
import com.redditapi.mongo.redditapi.utils.ApiResponse;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class VoteService {

    @Autowired
    private VoteRepository voteRepository;


    @Autowired
    private ValidationService validationService;

    public ApiResponse<VoteDTO> createVote(@Valid VoteDTO vote) {
        ApiResponse<Void> validationResponse = validationService.validateVote(vote);
        if (validationResponse.getStatus() != 200) {
            return new ApiResponse<>(validationResponse.getStatus(), validationResponse.getMessage(), null);
        }

        vote.setCreatedAt(new Date());
        return new ApiResponse<>(201, "Vote created successfully", voteRepository.save(vote));
    }

    public ApiResponse<List<VoteDTO>> getAllVotes() {
        return new ApiResponse<>(200, "Success", voteRepository.findAll());
    }

    public ApiResponse<VoteDTO> getVoteById(String id) {
        Optional<VoteDTO> optionalVote = voteRepository.findById(id);
        return optionalVote.map(vote -> new ApiResponse<>(200, "Success", vote))
                .orElseGet(() -> new ApiResponse<>(404, "Vote not found", null));
    }

    public ApiResponse<Void> deleteVote(String id) {
        voteRepository.deleteById(id);
        return new ApiResponse<>(204, "Vote deleted successfully", null);
    }
}
