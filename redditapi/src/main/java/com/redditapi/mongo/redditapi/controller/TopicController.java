package com.redditapi.mongo.redditapi.controller;

import com.redditapi.mongo.redditapi.model.DTO.TopicDTO;
import com.redditapi.mongo.redditapi.service.TopicService;
import com.redditapi.mongo.redditapi.utils.ApiResponse;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/topics")
@Validated
public class TopicController {

    @Autowired
    private TopicService topicService;

    @PostMapping
    public ApiResponse<TopicDTO> createTopic(@Valid @RequestBody TopicDTO topic) {
        return topicService.createTopic(topic);
    }

    @GetMapping
    public ApiResponse<List<TopicDTO>> getAllTopics() {
        return topicService.getAllTopics();
    }

    @GetMapping("/{id}")
    public ApiResponse<TopicDTO> getTopicById(@PathVariable String id) {
        return topicService.getTopicById(id);
    }

    @PutMapping("/{id}")
    public ApiResponse<TopicDTO> updateTopic(@PathVariable String id, @Valid @RequestBody TopicDTO topic) {
        return topicService.updateTopic(id, topic);
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> deleteTopic(@PathVariable String id) {
        return topicService.deleteTopic(id);
    }
}
