package com.redditapi.mongo.redditapi.model.DTO;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;


@Data
public class VoteDTO {
    @Id
    private String id;

    private int voteValue;

    private String UserId;

    private String idea;

    private Date createdAt;

}
