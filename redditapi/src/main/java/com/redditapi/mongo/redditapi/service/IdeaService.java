package com.redditapi.mongo.redditapi.service;

import com.redditapi.mongo.redditapi.model.DTO.IdeaDTO;
import com.redditapi.mongo.redditapi.repository.IdeaRepository;

import com.redditapi.mongo.redditapi.service.validation.ValidationService;
import com.redditapi.mongo.redditapi.utils.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class IdeaService {

    @Autowired
    private IdeaRepository ideaRepository;


    @Autowired
    private ValidationService validationService;

    public ApiResponse<IdeaDTO> createIdea(IdeaDTO idea) {
        ApiResponse<Void> validationResponse = validationService.validateIdea(idea);
        if (validationResponse.getStatus() != 200) {
            return new ApiResponse<>(validationResponse.getStatus(), validationResponse.getMessage(), null);
        }

        idea.setCreatedAt(new Date());
        return new ApiResponse<>(201, "Idea created successfully", ideaRepository.save(idea));
    }

    public ApiResponse<List<IdeaDTO>> getAllIdeas() {
        return new ApiResponse<>(200, "Success", ideaRepository.findAll());
    }

    public ApiResponse<IdeaDTO> getIdeaById(String id) {
        Optional<IdeaDTO> optionalIdea = ideaRepository.findById(id);
        return optionalIdea.map(idea -> new ApiResponse<>(200, "Success", idea))
                .orElseGet(() -> new ApiResponse<>(404, "Idea not found", null));
    }

    public ApiResponse<IdeaDTO> updateIdea(String id, IdeaDTO idea) {
        IdeaDTO existingIdea = getIdeaById(id).getData();
        if (existingIdea == null) {
            return new ApiResponse<>(404, "Idea not found", null);
        }

        ApiResponse<Void> validationResponse = validationService.validateIdea(idea);
        if (validationResponse.getStatus() != 200) {
            return new ApiResponse<>(validationResponse.getStatus(), validationResponse.getMessage(), null);
        }

        existingIdea.setDescription(idea.getDescription());
        existingIdea.setTopicId(idea.getTopicId());
        return new ApiResponse<>(200, "Idea updated successfully", ideaRepository.save(existingIdea));
    }

    public ApiResponse<Void> deleteIdea(String id) {
        Optional<IdeaDTO> optionalIdea = ideaRepository.findById(id);
        if (optionalIdea.isPresent()) {
            ideaRepository.deleteById(id);
            return new ApiResponse<>(204, "Idea deleted successfully", null);
        } else {
            return new ApiResponse<>(404, "Idea not found", null);
        }
    }
}
