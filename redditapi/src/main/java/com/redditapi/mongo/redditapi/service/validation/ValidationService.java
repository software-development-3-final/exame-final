package com.redditapi.mongo.redditapi.service.validation;

import com.redditapi.mongo.redditapi.model.DTO.IdeaDTO;
import com.redditapi.mongo.redditapi.model.DTO.TopicDTO;
import com.redditapi.mongo.redditapi.model.DTO.UserDTO;
import com.redditapi.mongo.redditapi.model.DTO.VoteDTO;
import com.redditapi.mongo.redditapi.repository.IdeaRepository;
import com.redditapi.mongo.redditapi.repository.TopicRepository;
import com.redditapi.mongo.redditapi.repository.UserRepository;
import com.redditapi.mongo.redditapi.utils.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


import java.util.Optional;

@Service
public class ValidationService {

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IdeaRepository ideaRepository;


    public ApiResponse<Void> validateTopic(TopicDTO topic) {
        if (StringUtils.isEmpty(topic.getTitle()) || StringUtils.isEmpty(topic.getDescription())) {
            return new ApiResponse<>(400, "Title and description cannot be empty", null);
        }
        return new ApiResponse<>(200, "Success", null);
    }

    public ApiResponse<Void> validateIdea(IdeaDTO idea) {
        if (StringUtils.isEmpty(idea.getDescription()) || idea.getTopicId() == null || idea.getUserId() == null) {
            return new ApiResponse<>(400, "Description, topic, and user cannot be empty", null);
        }

        // Verifica se o tópico existe
        Optional<TopicDTO> optionalTopic = topicRepository.findById(idea.getTopicId());
        if (!optionalTopic.isPresent()) {
            return new ApiResponse<>(404, "Topic not found", null);
        }

        // Verifica se o usuário existe
        Optional<UserDTO> optionalUser = userRepository.findById(idea.getUserId());
        if (!optionalUser.isPresent()) {
            return new ApiResponse<>(404, "User not found", null);
        }
        return new ApiResponse<>(200, "Success", null);
    }

    public ApiResponse<Void> validateVote(VoteDTO vote) {
        if (vote.getIdea() == null || vote.getUserId() == null || (vote.getVoteValue() != -1 && vote.getVoteValue() != 1)) {
            return new ApiResponse<>(400, "Invalid vote", null);
        }

        // Verifica se a ideia existe
        Optional<IdeaDTO> optionalIdea = ideaRepository.findById(vote.getIdea());
        if (!optionalIdea.isPresent()) {
            return new ApiResponse<>(404, "Idea not found", null);
        }

        // Verifica se o usuário existe
        Optional<UserDTO> optionalUser = userRepository.findById(vote.getUserId());
        if (!optionalUser.isPresent()) {
            return new ApiResponse<>(404, "User not found", null);
        }
        return new ApiResponse<>(200, "Success", null);
    }
}
